/* compute optimal solutions for sliding block puzzle. */
#include <SDL2/SDL.h>
#include <stdio.h>
#include <cstdlib>   /* for atexit() */
#include <algorithm>
using std::swap;
#include <cassert>
#include <iostream>
using std::cout;
#include <vector>
using std::vector;
#include <queue>
using std::queue;

/* SDL reference: https://wiki.libsdl.org/CategoryAPI */

/* initial size; will be set to screen size after window creation. */
int SCREEN_WIDTH = 640;
int SCREEN_HEIGHT = 480;
int fcount = 0;
int mousestate = 0;
SDL_Point lastm = {0,0}; /* last mouse coords */
SDL_Rect bframe; /* bounding rectangle of board */
static const int ep = 2; /* epsilon offset from grid lines */
int uw = bframe.w/4;
int uh = bframe.h/5;
//int xx = bframe.x + i*(bframe.w/4) + ep;
//int yy = bframe.y + j*(bframe.h/5) + ep;

bool init(); /* setup SDL */
void initBlocks();
void initConfig(); //initial config

void getPosition();
vector<int> myBoard(vector<int>& V); //put blocks in vector
void printBoard(const vector<int> V); //print for case p

vector<int> myBoard(vector<int>& V);
vector<int> V;

#define FULLSCREEN_FLAG SDL_WINDOW_FULLSCREEN_DESKTOP
// #define FULLSCREEN_FLAG 0

/* NOTE: ssq == "small square", lsq == "large square" */
enum bType {hor=1,ver=2,ssq=3,lsq=4};
struct block {
	SDL_Rect R; /* screen coords + dimensions */
	bType type; /* shape + orientation */
	int index;
	/* TODO: you might want to add other useful information to
	 * this struct, like where it is attached on the board.
	 * (Alternatively, you could just compute this from R.x and R.y,
	 * but it might be convenient to store it directly.) */
	void rotate() /* rotate rectangular pieces */
	{
		if (type != hor && type != ver) return;
		type = (type==hor)?ver:hor;
		swap(R.w,R.h);
	}
};

#define NBLOCKS 10
block B[NBLOCKS];
block* dragged = NULL;

block* findBlock(int x, int y);
void close(); /* call this at end of main loop to free SDL resources */
SDL_Window* gWindow = 0; /* main window */
SDL_Renderer* gRenderer = 0;

bool init()
{
	if(SDL_Init(SDL_INIT_VIDEO) < 0) {
		printf("SDL_Init failed.  Error: %s\n", SDL_GetError());
		return false;
	}
	/* NOTE: take this out if you have issues, say in a virtualized
	 * environment: */
	if(!SDL_SetHint(SDL_HINT_RENDER_VSYNC, "1")) {
		printf("Warning: vsync hint didn't work.\n");
	}
	/* create main window */
	gWindow = SDL_CreateWindow("Sliding block puzzle solver",
								SDL_WINDOWPOS_UNDEFINED,
								SDL_WINDOWPOS_UNDEFINED,
								SCREEN_WIDTH, SCREEN_HEIGHT,
								SDL_WINDOW_SHOWN|FULLSCREEN_FLAG);
	if(!gWindow) {
		printf("Failed to create main window. SDL Error: %s\n", SDL_GetError());
		return false;
	}
	/* set width and height */
	SDL_GetWindowSize(gWindow, &SCREEN_WIDTH, &SCREEN_HEIGHT);
	/* setup renderer with frame-sync'd drawing: */
	gRenderer = SDL_CreateRenderer(gWindow, -1,
			SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if(!gRenderer) {
		printf("Failed to create renderer. SDL Error: %s\n", SDL_GetError());
		return false;
	}
	SDL_SetRenderDrawBlendMode(gRenderer,SDL_BLENDMODE_BLEND);

	initBlocks();
	return true;
}

/* TODO: you'll probably want a function that takes a state / configuration
 * and arranges the blocks in accord.  This will be useful for stepping
 * through a solution.  Be careful to ensure your underlying representation
 * stays in sync with what's drawn on the screen... */

void initBlocks()
{
	int& W = SCREEN_WIDTH;
	int& H = SCREEN_HEIGHT;
	int h = H*3/4;
	int w = 4*h/5;
	int u = h/5-2*ep;
	int mw = (W-w)/2;
	int mh = (H-h)/2;

	/* setup bounding rectangle of the board: */
	bframe.x = (W-w)/2;
	bframe.y = (H-h)/2;
	bframe.w = w;
	bframe.h = h;

	/* NOTE: there is a tacit assumption that should probably be
	 * made explicit: blocks 0--4 are the rectangles, 5-8 are small
	 * squares, and 9 is the big square.  This is assumed by the
	 * drawBlocks function below. */

	for (size_t i = 0; i < 5; i++) {
		B[i].R.x = (mw-2*u)/2;
		B[i].R.y = mh + (i+1)*(u/5) + i*u;
		B[i].R.w = 2*(u+ep);
		B[i].R.h = u;
		B[i].type = hor;
	}
	B[4].R.x = mw+ep;
	B[4].R.y = mh+ep;
	B[4].R.w = 2*(u+ep);
	B[4].R.h = u;
	B[4].type = hor;
	/* small squares */
	for (size_t i = 0; i < 4; i++) {
		B[i+5].R.x = (W+w)/2 + (mw-2*u)/2 + (i%2)*(u+u/5);
		B[i+5].R.y = mh + ((i/2)+1)*(u/5) + (i/2)*u;
		B[i+5].R.w = u;
		B[i+5].R.h = u;
		B[i+5].type = ssq;
	}
	B[9].R.x = B[5].R.x + u/10;
	B[9].R.y = B[7].R.y + u + 2*u/5;
	B[9].R.w = 2*(u+ep);
	B[9].R.h = 2*(u+ep);
	B[9].type = lsq;
}

void initConfig() //for initial configuration at start
{
	int uw = bframe.w/4;
	int uh = bframe.h/5;

	//rotate rectangles if overlapping

	//hor and ver
	B[0].R.x = bframe.x + 0*uw+ep; //column
	B[0].R.y = bframe.y + 0*uh+ep; //row
	B[0].index = 0; //
	B[0].rotate();

	B[1].R.x = bframe.x + 3*uw+ep; //column
	B[1].R.y = bframe.y + 0*uh+ep; //row
	B[1].index = 3;//
	B[1].rotate();

	B[2].R.x = bframe.x + 0*uw+ep; //column
	B[2].R.y = bframe.y + 2*uh+ep; //row
	B[2].index = 8;
	B[2].rotate();

	B[3].R.x = bframe.x + 1*uw+ep; //column
	B[3].R.y = bframe.y + 2*uh+ep; //row
	B[3].index = 9;

	B[4].R.x = bframe.x + 3*uw+ep;
	B[4].R.y = bframe.y + 2*uh+ep;
	B[4].index = 10;
	B[4].rotate();

	//ssq
	B[5].R.x = bframe.x + 1*uw+ep;
	B[5].R.y = bframe.y + 3*uh+ep;
	B[5].index = 14;

	B[6].R.x = bframe.x + 2*uw+ep;
	B[6].R.y = bframe.y + 3*uh+ep;
	B[6].index = 15;

	B[7].R.x = bframe.x + 0*uw+ep;
	B[7].R.y = bframe.y + 4*uh+ep;
	B[7].index = 17;

	B[8].R.x = bframe.x + 3*uw+ep;
	B[8].R.y = bframe.y + 4*uh+ep;
	B[8].index = 19;

	//lsq
	B[9].R.x = bframe.x + 1*uw+ep;
	B[9].R.y = bframe.y + 0*uh+ep;
	B[9].index = 1;
}

vector<int> EI; //empty index
void getEmptyIndex(const vector<int> V, vector<int>& EI)
{
	EI.clear();
	for (int i = 0; i < 20; i++){
		if (V[i]==0) EI.push_back(i);
	}
	//cout << "Empty: ";
	//or (int i = 0; i<2; i++){
	//	cout << EI[i] << ' ';
	//}
	//cout <<'\n';
}

vector<int>CM; //can move index
void getCanMoveIndex(vector<int> EI, vector<int>& CM)
{
	CM.clear();
	switch(EI[0]){
		case 0:
			switch(EI[1]){
				case 1:
					CM.push_back(2);
					CM.push_back(4);
					CM.push_back(5);
					break;
				case 4:
					CM.push_back(1);
					CM.push_back(5);
					CM.push_back(8);
					break;
				case 9:
					CM.push_back(1);
					CM.push_back(4);
					CM.push_back(5);
					CM.push_back(8);
					CM.push_back(10);
					CM.push_back(13);
					break;
			}
			break;
		case 1:
			switch(EI[1]){
				case 2:
					CM.push_back(0);
					CM.push_back(3);
					CM.push_back(5);
					CM.push_back(6);
					break;
				case 5:
					CM.push_back(0);
					CM.push_back(2);
					CM.push_back(4);
					CM.push_back(6);
					CM.push_back(9);
					break;
				case 6:
					CM.push_back(0);
					CM.push_back(2);
					CM.push_back(4);
					CM.push_back(5);
					CM.push_back(7);
					CM.push_back(10);
					break;
				case 9:
					CM.push_back(0);
					CM.push_back(2);
					CM.push_back(5);
					CM.push_back(8);
					CM.push_back(10);
					CM.push_back(13);
					break;
			}
			break;
		case 2:
			switch(EI[1]){
        case 3:
          CM.push_back(1);
          CM.push_back(6);
          CM.push_back(7);
          break;
        case 6:
          CM.push_back(1);
					CM.push_back(3);
					CM.push_back(5);
					CM.push_back(7);
					CM.push_back(10);
					break;
      }
      break;
    case 3:
      switch(EI[1]){
        case 7:
          CM.push_back(2);
					CM.push_back(6);
					CM.push_back(11);
					break;
      }
      break;
    case 4:
      switch(EI[1]){
        case 5:
          CM.push_back(0);
					CM.push_back(1);
					CM.push_back(6);
					CM.push_back(8);
					CM.push_back(9);
					break;
        case 8:
          CM.push_back(0);
					CM.push_back(5);
					CM.push_back(9);
					CM.push_back(12);
					break;
      }
      break;
    case 5:
      switch(EI[1]){
        case 6:
          CM.push_back(1);
					CM.push_back(2);
					CM.push_back(4);
					CM.push_back(7);
					CM.push_back(9);
					CM.push_back(10);
          break;
        case 9:
          CM.push_back(1);
					CM.push_back(4);
					CM.push_back(6);
					CM.push_back(8);
					CM.push_back(10);
					CM.push_back(13);
          break;
      }
      break;
    case 6:
      switch(EI[1]){
        case 7:
          CM.push_back(2);
					CM.push_back(3);
					CM.push_back(5);
					CM.push_back(10);
					CM.push_back(11);
					break;
        case 10:
          CM.push_back(2);
					CM.push_back(5);
					CM.push_back(7);
					CM.push_back(9);
					CM.push_back(11);
					CM.push_back(14);
          break;
      }
      break;
    case 7:
      switch(EI[1]){
        case 11:
          CM.push_back(3);
					CM.push_back(6);
					CM.push_back(10);
					CM.push_back(15);
					break;
      }
      break;
    case 8:
      switch(EI[1]){
        case 9:
          CM.push_back(4);
					CM.push_back(5);
					CM.push_back(10);
					CM.push_back(12);
					CM.push_back(13);
					break;
        case 12:
          CM.push_back(4);
					CM.push_back(9);
					CM.push_back(13);
					CM.push_back(16);
					break;
        case 11:
          CM.push_back(4);
					CM.push_back(7);
					CM.push_back(9);
					CM.push_back(10);
					CM.push_back(12);
					CM.push_back(15);
          break;
        case 16:
          CM.push_back(4);
					CM.push_back(9);
					CM.push_back(12);
					CM.push_back(17);
					break;
        case 17:
          CM.push_back(4);
					CM.push_back(9);
					CM.push_back(12);
					CM.push_back(13);
					CM.push_back(16);
					CM.push_back(18);
          break;
      }
      break;
    case 9:
      switch(EI[1]){
        case 10:
          CM.push_back(5);
					CM.push_back(6);
					CM.push_back(8);
					CM.push_back(11);
					CM.push_back(13);
					CM.push_back(14);
          break;
        case 13:
          CM.push_back(5);
					CM.push_back(8);
					CM.push_back(10);
					CM.push_back(12);
					CM.push_back(14);
					CM.push_back(17);
          break;
        case 14:
          CM.push_back(5);
					CM.push_back(8);
					CM.push_back(10);
					CM.push_back(13);
					CM.push_back(15);
					CM.push_back(18);
        case 16:
          CM.push_back(5);
					CM.push_back(8);
					CM.push_back(10);
					CM.push_back(12);
					CM.push_back(13);
					CM.push_back(17);
          break;
        case 17:
          CM.push_back(5);
					CM.push_back(8);
					CM.push_back(10);
					CM.push_back(13);
					CM.push_back(16);
					CM.push_back(18);
          break;
        case 18:
          CM.push_back(5);
					CM.push_back(8);
					CM.push_back(10);
					CM.push_back(13);
					CM.push_back(14);
					CM.push_back(17);
          CM.push_back(19);
          break;
      }
      break;
    case 10:
      switch(EI[1]){
        case 11:
          CM.push_back(6);
					CM.push_back(7);
					CM.push_back(9);
					CM.push_back(14);
					CM.push_back(15);
					break;
        case 14:
          CM.push_back(6);
					CM.push_back(9);
					CM.push_back(11);
					CM.push_back(13);
					CM.push_back(15);
					break;
      }
      break;
    case 11:
      switch(EI[1]){
        case 15:
					CM.push_back(7);
					CM.push_back(10);
					CM.push_back(14);
					CM.push_back(19);
					break;
        case 18:
          CM.push_back(7);
					CM.push_back(10);
					CM.push_back(14);
					CM.push_back(15);
					CM.push_back(17);
					CM.push_back(19);
          break;
      }
      break;
    case 12:
      switch(EI[1]){
        case 13:
          CM.push_back(8);
					CM.push_back(9);
					CM.push_back(14);
					CM.push_back(16);
					CM.push_back(17);
          break;
        case 16:
          CM.push_back(8);
					CM.push_back(13);
					CM.push_back(14);
					break;
      }
      break;
    case 13:
      switch(EI[1]){
        case 14:
          CM.push_back(9);
					CM.push_back(10);
					CM.push_back(12);
					CM.push_back(15);
					CM.push_back(17);
					CM.push_back(18);
          break;
        case 17:
          CM.push_back(9);
					CM.push_back(12);
					CM.push_back(14);
					CM.push_back(16);
					CM.push_back(18);
          break;
      }
      break;
    case 14:
      switch(EI[1]){
        case 15:
					CM.push_back(10);
					CM.push_back(11);
					CM.push_back(13);
					CM.push_back(18);
					CM.push_back(19);
          break;
        case 18:
					CM.push_back(10);
					CM.push_back(13);
					CM.push_back(15);
					CM.push_back(17);
					CM.push_back(19);
          break;
      }
      break;
    case 15:
      switch(EI[1]){
        case 19:
          CM.push_back(11);
					CM.push_back(14);
					CM.push_back(18);
          break;
      }
    case 16:
      switch(EI[1]){
        case 17:
					CM.push_back(12);
					CM.push_back(13);
					CM.push_back(18);
          break;
      }
      break;
    case 17:
      switch(EI[1]){
        case 18:
					CM.push_back(13);
					CM.push_back(14);
					CM.push_back(16);
					CM.push_back(19);
          break;
      }
      break;
    case 18:
      switch(EI[1]){
        case 19:
					CM.push_back(11);
					CM.push_back(14);
					CM.push_back(18);
          break;
      }
      break;
	}
	cout << "Can Move: ";
	for (size_t i = 0; i < CM.size(); i++){
		cout << CM[i] << ' ';
	}
	cout << '\n';
}

void printList(vector<vector<int>> list){
	int x=-1;
	for(vector<vector<int>>::iterator i=list.begin();i!=list.end();i++){
		x++;
		printBoard(*i);
		printf("Steps: %i\n\n",x);
		//cout << '\n';
	}
}

vector<vector<int>> listOfCan; //list of new candidates for BFS
/*vector<vector<int>> myNewList(vector<int> EI, vector<int> CM,vector<int> V, vector<vector<int>>& L)
{
	L.clear(); //empty list first
	vector<int> C; //copy vector
	size_t i;
	switch(EI[0]){
		case 0:
			switch(EI[1]){
				//0-1
				case 1:
					for (i=0; i<CM.size(); i++){
						C = V;
						if (CM[i]==2){ //check if index is 2
							if (C[2] == hor){ //check type of block in index
								swap(C[1],C[3]); //move hor one space
								L.push_back(C); //add to list
								swap(C[0],C[2]); //move hor 2 spaces
								L.push_back(C); //add to list
							}else if(C[2]==ssq){
								swap(C[2],C[1]); //move small sqare 1 space
								L.push_back(C);
								swap(C[1],C[0]); //move small square 2 spaces
								L.push_back(C);
							}
						}
						else if(CM[i] == 4){//check index 4
							if (C[4]==ver){
								swap(C[0],C[8]);
								L.push_back(C);
							}else if(C[4] == ssq){
								swap(C[0],C[4]); //move one space
								L.push_back(C);
								swap(C[0],C[1]); //move 2 spaces
								L.push_back(C);
							}else if(C[4] == hor){
								swap(C[0],C[4]);
								swap(C[1],C[5]);
								L.push_back(C);
							}else {//if large sqare
								swap(C[0],C[8]);
								swap(C[1],C[9]);
								L.push_back(C);
							}
						}
						else if(CM[i] == 5){//check index 5
							if (C[5]==ver){
								swap(C[1],C[9]);
								L.push_back(C);
							}else if(C[5]==ssq){
								swap(C[1],C[5]);
								L.push_back(C);
								swap(C[1],C[0]);
								L.push_back(C);
							}
						}
					}//end loop
					break;
				//0-4
				case 4:
					for (i=0; i<CM.size(); i++){
						C = V;
						if (CM[i]==1){ //check if index is 1
							if (C[1]==ver){
								swap(C[0],C[1]);
								swap(C[4],C[5]);
								L.push_back(C);
							}else if (C[1] == hor){ //check type of block in index
								swap(C[0],C[2]); //move hor 2 spaces
								L.push_back(C); //add to list
							}else if(C[1]==ssq){
								swap(C[0],C[1]); //move small sqare 1 space
								L.push_back(C);
								swap(C[4],C[0]); //move small square 2 spaces
								L.push_back(C);
							}else if (C[1]==lsq){
								swap(C[0],C[2]);
								swap(C[4],C[6]);
								L.push_back(C);
							}
						}
						else if(CM[i] == 5){//check index 5
							if (C[5]==hor){
								swap(C[4],C[6]);
								L.push_back(C);
							}else if(C[5] == ssq){
								swap(C[5],C[4]); //move one space
								L.push_back(C);
								swap(C[0],C[4]); //move 2 spaces
								L.push_back(C);
							}
						}
						else if(CM[i] == 8){//check index 8
							if (C[8]==ver){
								swap(C[12],C[4]);
								L.push_back(C);
								swap(C[8],C[0]);
								L.push_back(C);
							}else if(C[8]==ssq){
								swap(C[4],C[8]);
								L.push_back(C);
								swap(C[4],C[0]);
								L.push_back(C);
							}
						}
					}//e
					break;
				//0-9
				case 9:
					for (i=0; i<CM.size(); i++){
						C = V;
						if (CM[i]==1){ //check if index is 1
							if (C[1] == hor){ //check type of block in index
								swap(C[0],C[2]); //move hor one space
								L.push_back(C); //add to list
							}else if(C[1]==ssq){
								swap(C[0],C[1]); //move small sqare 1 space
								L.push_back(C);
							}
						}
						else if(CM[i] == 4){//check index 4
							if (C[4]==ver){
								swap(C[0],C[8]);
								L.push_back(C);
							}else if(C[4] == ssq){
								swap(C[0],C[4]); //move one space
								L.push_back(C);
								swap(C[0],C[1]); //move 2 spaces
								L.push_back(C);
							}else if(C[4] == hor){
								swap(C[0],C[4]);
								swap(C[1],C[5]);
								L.push_back(C);
							}else {//if large sqare
								swap(C[0],C[8]);
								swap(C[1],C[9]);
								L.push_back(C);
							}
						}
						else if(CM[i] == 5){//check index 5
							if (C[5]==ver){
								swap(C[1],C[9]);
								L.push_back(C);
							}else if(C[5]==ssq){
								swap(C[1],C[5]);
								L.push_back(C);
								swap(C[1],C[0]);
								L.push_back(C);
							}
						else if(CM[i] == 8){
							if (C[8]==ssq){
								swap(C[8],C[9]);
								L.push_back(C);
							}
						}
						else if (CM[i]==10){
							if (C[10]==ssq){
								swap(C[10],C[9]);
								L.push_back(C);
							}else if (C[10]==hor){
								swap(C[11],C[9]);
								L.push_back(C);
						}
						else if (CM[i]==13){
							if (C[13]==ssq){
								swap(C[13],C[9]);
								L.push_back(C);
							}else if (C[13]==ver){
								swap(C[9],C[17]);
								L.push_back(C);
							}
						}
					}//end loop
					break;
			}
			break;
		//case 1
		case 1:
			switch(EI[1]){
				//1-2
				case 2:
					for (i=0; i<CM.size(); i++){
						C = V;
						if (CM[i]==0){ //check if index is 0
							if (C[0] == ssq){ //check type of block in index
								swap(C[1],C[0]); //move ssq one space
								L.push_back(C); //add to list
								swap(C[1],C[2]); //move ssq 2 spaces
								L.push_back(C); //add to list
							}
						}
						else if(CM[i]==3){//check index 3
							if (C[3] == ssq){
								swap(C[3],C[2]);
								L.push_back(C);
								swap(C[1],C[2]);
								L.push_back(C);
							}
						}
						else if(CM[i] == 5){//check index 5
							if (C[5]==ver){
								swap(C[1],C[9]);
								L.push_back(C);
							}else if(C[5]==ssq){
								swap(C[1],C[5]);
								L.push_back(C);
								swap(C[1],C[2]);
								L.push_back(C);
							}else if (C[5]==hor){
								swap(C[5],C[1]);
								swap(C[6],C[2]);
								L.push_back(C);
							}else if (C[5]==lsq){
								swap(C[1],C[9]);
								swap(C[2],C[10]);
								L.push_back(C);
							}
						}
						else if (CM[i] == 6){
							if(C[6]==ver){
								swap(C[2],C[10]);
								L.push_back(C);
							}else if(C[6]==ssq){
								swap(C[6],C[2]);
								L.push_back(C);
								swap(C[1],C[2]);
								L.push_back(C);
							}
						}
					}//end loop
					break;
				//1-5
				case 5:
					for (i=0; i<CM.size(); i++){
						C = V;
						if (CM[i]==0){ //check if index is 0
							if (C[0]==ver){
								swap(C[0],C[1]);
								swap(C[4],C[5]);
								L.push_back(C);
							}else if(C[0]==ssq){
								swap(C[0],C[1]); //move small sqare 1 space
								L.push_back(C);
								swap(C[5],C[0]); //move small square 2 spaces
								L.push_back(C);
							}
						}
						else if(CM[i] == 2){//check index 2
							if (C[2]==ver){
								swap(C[2],C[1]);
								swap(C[5],C[6]);
								L.push_back(C);
							}else if(C[2] == ssq){
								swap(C[2],C[1]); //move one space
								L.push_back(C);
								swap(C[1],C[5]); //move 2 spaces
								L.push_back(C);
							}else if(C[2]==hor){
								swap(C[3],C[1]);
								L.push_back(C);
						}
						else if(CM[i] == 4){//check index 4
							if(C[4]==ssq){
								swap(C[4],C[5]);
								L.push_back(C);
								swap(C[5],C[1]);
								L.push_back(C);
							}
						}
						else if(CM[i] == 6){//check index 4
							if(C[6]==ssq){
								swap(C[4],C[5]);
								L.push_back(C);
								swap(C[5],C[1]);
								L.push_back(C);
							}else if(C[6]==hor){
								swap(C[5],C[7]);
								L.push_back(C);
							}
						}
						else if(CM[i]==9){//check index 9
							if (C[9]==ver){
								swap(C[13],C[5]);
								L.push_back(C);
								swap(C[9],C[1]);
								L.push_back(C);
							}else if(C[9]==ssq){
								swap(C[9],C[5]);
								L.push_back(C);
								swap(C[5],C[1]);
								L.push_back(C);
							}
						}
					}//e
					break;
				//1-6
				case 6:
					for (i=0; i<CM.size(); i++){
						C = V;
						if (CM[i]==0){ //check if index is 0
							if (C[1] == ssq){ //check type of block in index
								swap(C[0],C[1]); //move hor one space
								L.push_back(C); //add to list
							}
						}
						else if(CM[i]==2){
							if (C[2] == ssq){ //check type of block in index
								swap(C[2],C[1]);
								L.push_back(C);
								swap(C[2],C[1]);
								swap(C[2],C[6]);
								L.push_back(C);
							}else if(C[2]==hor){
								swap(C[3],C[1]);
								L.push_back(C);
							}
						}
						else if(CM[i] == 5){//check index 5
							if (C[5]==ver){
								swap(C[1],C[9]);
								L.push_back(C);
							}else if(C[5] == ssq){
								swap(C[5],C[1]); //move one space
								L.push_back(C);
								swap(C[5],C[1]);
								swap(C[5],C[6]);
								L.push_back(C);
							}else if(C[5] == hor){
								swap(C[6],C[4]);
								L.push_back(C);
							}
						}
						else if(CM[i] == 7){//check index 7
							if(C[7]==ssq){
								swap(C[6],C[7]);
								L.push_back(C);
							}
						else if(CM[i] == 10){
							if (C[10]==ssq){
								swap(C[10],C[6]);
								L.push_back(C);
							}else if(C[10]==hor){
								swap(C[14],C[6]);
								L.push_back(C);
							}
						}
					}//end loop
					break;
				//1-9
				case 9:
					for (i=0; i<CM.size(); i++){
						C = V;
						if (CM[i]==0){ //check if index is 0
							if (C[1] == ssq){ //check type of block in index
								swap(C[0],C[1]); //move hor one space
								L.push_back(C); //add to list
							}
						}
						if (CM[i]==2){ //check if index is 2
							if (C[2]==ssq){
								swap(C[2],C[1]); //move small square 1 space
								L.push_back(C);
							}else if (C[2] == hor){ //check type of block in index
								swap(C[1],C[3]);
								L.push_back(C);
							}
						}
						else if(CM[i] == 5){//check index 5
							if(C[5] == ssq){
								swap(C[5],C[1]); //move one space
								L.push_back(C);
								swap(C[5],C[1]);
								swap(C[5],C[9]);
								L.push_back(C);
							}
						}
						else if (CM[i]==8){ //check if index is 0
							if (C[8] == ssq){ //check type of block in index
								swap(C[8],C[9]); //move hor one space
								L.push_back(C); //add to list
							}
						}
						else if(CM[i] == 10){//check index 8
							if (C[10]==hor){
								swap(C[11],C[9]);
								L.push_back(C);
							}else if(C[10]==ssq){
								swap(C[10],C[9]);
								L.push_back(C);
							}
						}
						else if(CM[i] == 13){
							if (C[13]==ver){
								swap(C[17],C[9]);
								L.push_back(C);
							}else if (C[13]==ssq){
								swap(C[13],C[9]);
								L.push_back(C);
							}
						}
					}//e
					break;
			}
			break;
		//case 2
		case 2:
			switch(EI[1]){
				//2-3
				case 3:
					for (i = 0; i < CM.size();i++){
						C=V;
						if(CM[i]==1){
							if (C[1]==hor){
								swap(C[0],C[2]);
								L.push_back(C);
								swap(C[1],C[3]);
								L.push_back(C);
							}else if(C[1]==ssq){
								swap(C[1],C[2]);
								L.push_back(C);
								swap(C[2],C[3]);
								L.push_back(C);
							}
						}
						else if(CM[i]==6){
							if(C[6]==hor){
								swap(C[6],C[2]);
								swap(C[3],C[7]);
								L.push_back(C);
							}else if(C[6]==ver){
								swap(C[10],C[2]);
								L.push_back(C);
							}else if(C[6]==ssq){
								swap(C[6],C[2]);
								L.push_back(C);
								swap(C[2],C[3]);
								L.push_back(C);
							}else if(C[6]==lsq){
								swap(C[10],C[2]);
								swap(C[11],C[3]);
								L.push_back(C);
							}
						}
						else if(CM[i] == 7){
							if(C[7]==ver){
								swap(C[11],C[3]);
								L.push_back(C);
							}else if (C[7]==ssq){
								swap(C[7],C[3]);
								L.push_back(C);
								swap(C[3],C[2]);
								L.push_back(C);
							}
						}
					}//endloop
					break;
				//2-6
				case 6:
					for (i = 0; i < CM.size();i++){
						C=V;
						if(CM[i]==1){
							if (C[1]==hor){
								swap(C[0],C[2]);
								L.push_back(C);
							}else if(C[1]==ssq){
								swap(C[1],C[2]);
								L.push_back(C);
								swap(C[2],C[6]);
								L.push_back(C);
							}else if(C[1]==ver){
								swap(C[1],C[2]);
								swap(C[5],C[6]);
								L.push_back(C);
							}else if(C[1]==lsq){
								swap(C[0],C[2]);
								swap(C[4],C[6]);
								L.push_back(C);
							}
						}
						else if(CM[i]==3){
							if(C[3]==ver){
								swap(C[2],C[3]);
								swap(C[6],C[7]);
								L.push_back(C);
							}else if(C[3]==ssq){
								swap(C[2],C[3]);
								L.push_back(C);
								swap(C[2],C[6]);
								L.push_back(C);
							}
						}
						else if(CM[i]==5){
							if(C[5]==hor){
								swap(C[6],C[4]);
								L.push_back(C);
							}else if(C[5]==ssq){
								swap(C[6],C[5]);
								L.push_back(C);
								swap(C[2],C[6]);
								L.push_back(C);
							}
						}
						else if(CM[i] == 7){
							if (C[7]==ssq){
								swap(C[7],C[6]);
								L.push_back(C);
								swap(C[6],C[2]);
								L.push_back(C);
							}
						}
						else if(CM[i]==10){
							if (C[10]==ver){
								swap(C[14],C[6]);
								L.push_back(C);
								swap(C[10],C[2]);
								L.push_back(C);
							}else if(C[10]==ssq){
								swap(C[6],C[10]);
								L.push_back(C);
								swap(C[2],C[6]);
								L.push_back(C);
							}
						}
					}//endloop
					break;
			}
			break;
		//case 3:
		//case 4:
		//case 5:
		//case 6:
		//case 7:
		//case 8:
		//case 9:
		//case 10:
		//case 11:
		//case 12:
		//case 13:
		//case 14:
		//case 15:
		//case 16:
		//case 17:
		//case 18:
	}
	return L;
}*/

//function to get possible candidates for DFS
vector<vector<int>> myMove(vector<int> EI,vector<int> CM,vector<int> V, vector<vector<int>>& L){
	//CM.clear();
	L.clear();
	vector<int> CP;//copy
	int SC = EI[0]*19 + EI[1]; //my equation to ShortCut cases
	switch(SC){
		case 1:
			CP=V; //ideally after forloop
			if(CP[2]==ssq && CP[4]==ssq){
				swap(CP[4],CP[1]);//mov 46
				L.push_back(CP);
			}
			break;
		case 2: //0 2
			break;
		case 3: //0 3
			break;
		case 4: //0 4
			CP=V; //ideally after forloop
			if(CP[1]==ver && CP[8]==ssq){
				swap(CP[0],CP[8]);//mov 30
				L.push_back(CP);
			}else if(CP[1]==ssq){
				swap(CP[0],CP[8]);//mov 47
				L.push_back(CP);
			}else if(CP[8]==ver){
				swap(CP[0],CP[8]);//mov 67
				swap(CP[4],CP[12]);
				L.push_back(CP);
			}
			break;
		case 5: //0 5
			break;
		case 6: //0 6
			break;
		case 7: //0 7
			break;
		case 8: //0 8
			break;
		case 9: //0 9
			CP=V; //ideally after forloop
			if(CP[1]==ssq){
				swap(CP[0],CP[8]);//mov 58
				L.push_back(CP);
			}
			break;
		case 10: //0 10
			break;
		case 11: //0 11
			break;
		case 12: //0 12
			break;
		case 13: //0 13
			break;
		case 14: //0 14
			break;
		case 15: //0 15
			break;
		case 16: //0 16
			break;
		case 17: //0 17
			break;
		case 18: //0 18
			break;
		case 19: //0 19
			break;
		//case 1
		case 21: //1 2
			CP=V; //ideally after forloop
			if(CP[0]==ssq && CP[3]==ver){
				swap(CP[0],CP[2]);//mov 45
				L.push_back(CP);
			}
			break;
		case 22: //1 3
			break;
		case 23: //1 4
			CP=V; //ideally after forloop
			if(CP[0]==ssq && CP[2]==lsq){
				swap(CP[1],CP[3]);//mov 34
				swap(CP[5],CP[7]);
				L.push_back(CP);
			}
			break;
		case 24: //1 5
			CP=V; //ideally after forloop
			if(CP[0]==ver){ //if CP[2]=lsq  if CP[9]=lsq
				swap(CP[1],CP[0]);//mov 29 //mov 66
				swap(CP[4],CP[5]);//
				L.push_back(CP);
			}else if(CP[0] == ssq && CP[2]==lsq&& CP[9]==ver){
				swap(CP[1],CP[3]);//move new
				swap(CP[5],CP[7]);
				L.push_back(CP);
			}
			break;
		case 25: //1 6
			CP=V; //ideally after forloop
			if(CP[5]==ssq&& CP[16]==ssq){
				swap(CP[5],CP[6]);//
				L.push_back(CP);
			}
			break;
		case 26: //1 7
			break;
		case 27: //1 8
			break;
		case 28: //1 9
			CP=V; //ideally after forloop
			if(CP[5]==ssq){
				swap(CP[0],CP[1]);//mov 57
				L.push_back(CP);
			}
			break;
		case 29: //1 10
			break;
		case 30: //1 11
			break;
		case 31: //1 12
			break;
		case 32: //1 13
			break;
		case 33: //1 14
			break;
		case 34: //1 15
			break;
		case 35: //1 16
			break;
		case 36: //1 17
			break;
		case 37: //1 18
			break;
		case 38: //1 19
			break;
		//case 2
		case 41: //2 3
			break;
		case 42: //2 4
			break;
		case 43: //2 5
			break;
		case 44: //2 6
			CP=V; //ideally after forloop
			if(CP[9]==lsq && CP[3]==ver && CP[16]==ver){
				swap(CP[2],CP[3]);//mov 52
				swap(CP[6],CP[7]);
				L.push_back(CP);
			}else if(CP[9]==lsq && CP[1]==ssq && CP[16]==ssq){
				swap(CP[1],CP[2]);//mov 65
				L.push_back(CP);
			}else if(CP[8]==lsq && (CP[2]==ver || CP[10]==ver)){
				swap(CP[2],CP[10]);//mov 72 //mov 76
				swap(CP[6],CP[14]);
				L.push_back(CP);
			}
			break;
		case 45: //2 7
			CP=V; //ideally after forloop
			if(CP[6]==ssq){
				swap(CP[7],CP[6]);//mov 71
				L.push_back(CP);
			}
			break;
		case 46: //2 8
			break;
		case 47: //2 9
			break;
		case 48: //2 10
			break;
		case 49: //2 11
			break;
		case 50: //2 12
			break;
		case 51: //2 13
			break;
		case 52: //2 14
			break;
		case 53: //2 15
			break;
		case 54: //2 16
			break;
		case 55: //2 17
			break;
		case 56: //2 18
			break;
		case 57: //2 19
			break;
		//case 3
		case 61: //3 4
			break;
		case 62: //3 5
			break;
		case 63: //3 6
			break;
		case 64: //3 7
			CP=V; //ideally after forloop
			if(CP[11]==ver && CP[2] == lsq && CP[0]==ver){
				swap(CP[3],CP[1]);//mov 28
				swap(CP[5],CP[7]);//
				L.push_back(CP);
			}else if(CP[8]==ver && CP[10]==ver && CP[0]==ssq){
				swap(CP[3],CP[11]);//mov 35
				swap(CP[7],CP[15]);
				L.push_back(CP);
			}else if(CP[10]==lsq && CP[4]==ver && CP[16]==ver){
				swap(CP[3],CP[11]);//mov 53
				swap(CP[7],CP[15]);
				L.push_back(CP);
			}else if(CP[9]==lsq && CP[16]==ssq && CP[2]==ver){
				swap(CP[2],CP[3]);//mov 64 //mov 75
				swap(CP[6],CP[7]);
				L.push_back(CP);
			}else if(CP[8]==lsq&& CP[2]==ssq){
				swap(CP[2],CP[3]);//mov 70
				L.push_back(CP);
			}
			break;
		case 65: //3 8
			break;
		case 66: //3 9
			break;
		case 67: //3 10
			break;
		case 68: //3 11
			break;
		case 69: //3 12
			break;
		case 70: //3 13
			break;
		case 71: //3 14
			break;
		case 72: //3 15
			break;
		case 73: //3 16
			break;
		case 74: //3 17
			break;
		case 75: //3 18
			break;
		case 76: //3 19
			break;
		//case 4
		case 81: //4 5
			break;
		case 82: //4 6
			break;
		case 83: //4 7
			break;
		case 84: //4 8
			CP=V; //ideally after forloop
			if(CP[0]==ssq && CP[12]==ssq){
				swap(CP[4],CP[12]);//mov 31
				L.push_back(CP);
			}else if(CP[0]==ssq && CP[12]==ver){
				swap(CP[4],CP[12]);//mov 48
				swap(CP[8],CP[16]);
				L.push_back(CP);
			}
			break;
		case 85: //4 9
			break;
		case 86: //4 10
			break;
		case 87: //4 11
			break;
		case 88: //4 12
			break;
		case 89: //4 13
			break;
		case 90: //4 14
			break;
		case 91: //4 15
			break;
		case 92: //4 16
			break;
		case 93: //4 17
			break;
		case 94: //4 18
			break;
		case 95: //4 19
			break;
		//case 5
		case 101: //5 6
			CP=V; //ideally after forloop
			if(CP[9]==lsq && CP[2]==ssq){
				swap(CP[2],CP[5]);//mov 52
				L.push_back(CP);
			}
			break;
		case 102: //5 7
			break;
		case 103: //5 8
			break;
		case 104: //5 9
			CP=V; //ideally after forloop
			if(CP[10]==lsq && CP[1] == ssq){
				swap(CP[5],CP[1]);//mov 56
				L.push_back(CP);
			}
			break;
		case 105: //5 10
			break;
		case 106: //5 11
			break;
		case 107: //5 12
			break;
		case 108: //5 13
			break;
		case 109: //5 14
			break;
		case 110: //5 15
			break;
		case 111: //5 16
			break;
		case 112: //5 17
			break;
		case 113: //5 18
			break;
		case 114: //5 19
			break;
		//case 6
		case 121: //6 7
			break;
		case 122: //6 8
			break;
		case 123: //6 9
			break;
		case 124: //6 10
			break;
		case 125: //6 11
			break;
		case 126: //6 12
			break;
		case 127: //6 13
			break;
		case 128: //6 14
			break;
		case 129: //6 15
			break;
		case 130: //6 16
			break;
		case 131: //6 17
			break;
		case 132: //6 18
			break;
		case 133: //6 19
			break;
		//case 7
		case 141: //7 8
			break;
		case 142: //7 9
			break;
		case 143: //7 10
			break;
		case 144: //7 11
			CP=V; //ideally after forloop
			if(CP[3]==ssq && CP[15]==ssq){
				swap(CP[3],CP[11]);//mov 74
				L.push_back(CP);
			}
			break;
		case 145: //7 12
			break;
		case 146: //7 13
			break;
		case 147: //7 14
			break;
		case 148: //7 15
			break;
		case 149: //7 16
			break;
		case 150: //7 17
			break;
		case 151: //7 18
			break;
		case 152: //7 19
			break;
		//case 8
		case 161: //8 9
			CP=V; //ideally after forloop
			if(CP[10]==hor){
				swap(CP[11],CP[9]);//mov11
				swap(CP[10],CP[8]);
				L.push_back(CP);
			}else if(CP[10]==ssq){ //cp[0]=hor cp[0]ssq
				swap(CP[10],CP[8]); //mov18 //mov 43 //mov 82
				L.push_back(CP);
			}else if(CP[10]==lsq){
				swap(CP[16],CP[8]);//mov 59
				L.push_back(CP);
			}
			break;
		case 162: //8 10
			break;
		case 163: //8 11
			break;
		case 164: //8 12
			CP=V;
			if(CP[16]==ssq&&CP[4]==ver && CP[9]!=lsq){
				swap(CP[16],CP[12]); //mov7
				L.push_back(CP);
			}else if(CP[16]==hor && CP[9]==ver){
				swap(CP[8],CP[9]); //mov 32
				swap(CP[12],CP[13]);
				L.push_back(CP);
			}else if(CP[16]==ssq && CP[9]==lsq){
				swap(CP[8],CP[10]);//mov 68
				swap(CP[12],CP[14]);
				L.push_back(CP);
			}
			break;
		case 165: //8 13
			break;
		case 166: //8 14
			break;
		case 167: //8 15
			break;
		case 168: //8 16
			CP=V;
			//if(CP[17]==ssq)
			swap(CP[16],CP[17]);//mov8
			L.push_back(CP);
			break;
		case 169: //8 17
			CP=V;
			if(CP[13]==ver){ //&& CP[0]==ver){ for mov 10 CP[0]ssq for 42
				swap(CP[9],CP[17]);//mov 10 //mov 42
				L.push_back(CP);
			}
			break;
		case 170: //8 18
			break;
		case 171: //8 19
			break;
		//case 9
		case 181: //9 10
			CP=V; //ideally after forloop
			if(CP[11]==ssq){
				swap(CP[11],CP[9]);//mov 19 //mov 83
				L.push_back(CP);
			}else if(CP[11]==ver && CP[0]==ssq){
				swap(CP[1],CP[9]); //mov 44
				swap(CP[2],CP[10]);
				L.push_back(CP);
			}
			break;
		case 182: //9 11
			break;
		case 183: //9 12
			break;
		case 184: //9 13
			CP=V;
			if(CP[8]==ver&&CP[10]==hor){
				swap(CP[8],CP[9]);//mov 6
				swap(CP[12],CP[13]);
				L.push_back(CP);
			}else if(CP[12]==ssq && CP[10]==ver){
				swap(CP[9],CP[10]); //mov 25
				swap(CP[13],CP[14]);
				L.push_back(CP);
			}else if(CP[12]==ver && CP[10]==ver && CP[17]==hor){
				swap(CP[1],CP[9]);//mov 33
				swap(CP[5],CP[13]);
				L.push_back(CP);
			}else if(CP[11]==lsq && CP[5]==ssq && CP[16]==ver){
				swap(CP[5],CP[13]); //mov 55
				L.push_back(CP);
			}else if(CP[11]==lsq && CP[17]==ssq && CP[16]==ssq){
				swap(CP[9],CP[11]); //mov 62
				swap(CP[13],CP[15]);
				L.push_back(CP);
			}
			break;
		case 185: //9 14
			CP=V;
			//if(CP[13]==ssq)
			swap(CP[13],CP[14]);//mov 5
			L.push_back(CP);
			break;
		case 186: //9 15
			break;
		case 187: //9 16
			CP=V; //ideally after forloop
			if(CP[13]==ssq){
				swap(CP[17],CP[16]);//mov 60
				L.push_back(CP);
			}
			break;
		case 188: //9 17
			CP=V; //ideally after forloop
			if(CP[16]==ssq){
				swap(CP[13],CP[17]);//mov 61
				L.push_back(CP);
			}
			break;
		case 189: //9 18
			CP=V;
			//if(CP[14]==ssq)
			swap(CP[18],CP[14]);//mov 4
			L.push_back(CP);
			break;
		case 190: //9 19
			break;
		//case 10
		case 201: //10 11
			CP=V; //ideally after forloop
			if(CP[14]==ssq){
				swap(CP[14],CP[11]);//mov12
				L.push_back(CP);
			}else if(CP[14]==ver){
				swap(CP[10],CP[18]);//mov19
				L.push_back(CP);
			}else if(CP[14]==hor){
				swap(CP[10],CP[14]);//mov 84
				swap(CP[11],CP[15]);
				L.push_back(CP);
			}
			break;
		case 202: //10 12
			break;
		case 203: //10 13
			break;
		case 204: //10 14
			CP=V; //ideally after forloop
			if(CP[18]==ssq&&CP[11]==ssq){
				swap(CP[18],CP[10]);//mov13
				L.push_back(CP);
			}else if(CP[18]==ssq && CP[11]==ver && CP[0]==ver){
				swap(CP[10],CP[11]);//mov26
				swap(CP[14],CP[15]);
				L.push_back(CP);
			}else if(CP[9]==ver && CP[0]==ssq && CP[18]==ssq){
				swap(CP[18],CP[10]);//mov 38
				L.push_back(CP);
			}else if (CP[18]==hor && CP[11]==ver){
				swap(CP[10],CP[11]);//mov 69 //mov 72
				swap(CP[15],CP[14]);
				L.push_back(CP);
			}else if(CP[18]==hor && CP[11]==ssq){
				swap(CP[10],CP[15]);//mov 77
				L.push_back(CP);
			}
			break;
		case 205: //10 15
			break;
		case 206: //10 16
			break;
		case 207: //10 17
			break;
		case 208: //10 18
			break;
		case 209: //10 19
			break;
		//case 11
		case 221: //11 12
			break;
		case 222: //11 13
			break;
		case 223: //11 14
			break;
		case 224: //11 15
			CP=V; //ideally after forloop
			if(CP[7]==ver && CP[10]==ver && CP[0]==ver){
				swap(CP[3],CP[11]);//mov 27 //mov 70
				swap(CP[7],CP[15]);//
				L.push_back(CP);
			}else if(CP[10]==ver && CP[0]==ssq && CP[19]==ssq){
				swap(CP[11],CP[10]);//mov 36
				swap(CP[15],CP[14]);
				L.push_back(CP);
			}else if(CP[10]==lsq && CP[14]==lsq && CP[16]==ver){
				swap(CP[9],CP[11]);//mov 54
				swap(CP[13],CP[15]);
				L.push_back(CP);
			}else if(CP[10]==lsq && CP[7]==ver && CP[16]==ssq){
				swap(CP[3],CP[11]);//mov 63
				swap(CP[7],CP[15]);
				L.push_back(CP);
			}else if(CP[10]==ver && CP[7]==ssq){
				swap(CP[7],CP[15]);//mov 73
				L.push_back(CP);
			}
			break;
		case 225: //11 16
			break;
		case 226: //11 17
			break;
		case 227: //11 18
			CP=V;
			if(CP[10]==hor){
				swap(CP[11],CP[9]);//move 3
				L.push_back(CP);
			}else if(CP[15]==ver && CP[10]==ver){
				swap(CP[19],CP[11]);//mov20
				L.push_back(CP);
			}
			break;
		case 228: //11 19
			break;
		//case 12
		case 241: //12 13
			CP=V; //ideally after forloop
			if(CP[8]==hor){
				swap(CP[12],CP[8]);//mov 17
				swap(CP[13],CP[9]);//
				L.push_back(CP);
			}else if(CP[8]==ssq && CP[16]==hor){
				swap(CP[9],CP[12]); //mov 24
				L.push_back(CP);
			}
			break;
		case 242: //12 14
			break;
		case 243: //12 15
			break;
		case 244: //12 16
			CP=V; //ideally after forloop
			if(CP[8]==ver && CP[13]==ver){
				swap(CP[13],CP[12]);//mov 49
				swap(CP[16],CP[17]);
				L.push_back(CP);
			}
			break;
		case 245: //12 17
			break;
		case 246: //12 18
			break;
		case 247: //12 19
			break;
		//case 13
		case 261: //13 14
			CP=V; //ideally after forloop
			if(CP[9]==lsq && CP[17]==ssq){
				swap(CP[5],CP[13]);//mov 51
				swap(CP[6],CP[14]);
				L.push_back(CP);
			}
			break;
		case 262: //13 15
			break;
		case 263: //13 16
			break;
		case 264: //13 17
			CP=V; //ideally after forloop
			if(CP[12]==ssq){
				swap(CP[12],CP[17]);//mov 16
				L.push_back(CP);
			}else if(CP[12]==ver && CP[14]==ssq){
				swap(CP[14],CP[17]);//mov
				L.push_back(CP);
			}
			break;
		case 265: //13 18
			break;
		case 266: //13 19
			break;
		//case 14
		case 281: //14 15
			CP=V; //ideally after forloop
			if(CP[18]==hor){
				swap(CP[14],CP[18]);//mov 78
				swap(CP[15],CP[19]);
				L.push_back(CP);
			}else if(CP[18]==ssq){
				swap(CP[18],CP[15]);//mov 85
				L.push_back(CP);
			}
			break;
		case 282: //14 16
			break;
		case 283: //14 17
			break;
		case 284: //14 18
			CP=V; //ideally after forloop
			if(CP[13]==ver && CP[0]==ver){
				swap(CP[14],CP[13]);//mov 15
				swap(CP[18],CP[17]);//
				L.push_back(CP);
			}else if(CP[10]==ssq && CP[0]==ssq && CP[19]==ssq){
				swap(CP[19],CP[14]);//mov 39
				L.push_back(CP);
			}else if(CP[13]==lsq){
				swap(CP[12],CP[14]);//mov 86
				swap(CP[16],CP[18]);
				L.push_back(CP);
			}
			break;
		case 285: //14 19
			break;
		//case 15
		case 301: //15 16
			break;
		case 302: //15 17
			break;
		case 303: //15 18
			break;
		case 304: //15 19
			break;
		//case 16
		case 321: //16 17
			CP=V; //ideally after forloop
			if(CP[12]==hor){
				swap(CP[12],CP[16]);//mov 23
				swap(CP[13],CP[17]);//
				L.push_back(CP);
			}else if(CP[12]==ver && CP[18]==hor){
				swap(CP[8],CP[16]);//mov 41
				L.push_back(CP);
			}else if(CP[12]==lsq){
				swap(CP[8],CP[16]);//mov 81
				swap(CP[9],CP[17]);
				L.push_back(CP);
			}
			break;
		case 322: //16 18
			break;
		case 323: //16 19
			break;
		//case 17
		case 341: //17 18
			CP=V;
			//for(i = 0; i<CM.size();i++)
			//if (CM[i]== 13..14..16..19)
			if (CP[19]==ssq && CP[14]==ssq){
				swap(CP[19],CP[17]); //move 1
				L.push_back(CP);
			}else if(CP[16]==ssq && (CP[14]==ver || CP[14]==hor)){
				swap(CP[16],CP[18]); //mov 22 //mov 80
				L.push_back(CP);
			}
			break;
		case 342: //17 19
			break;
		//case 18
		case 361: //18 19
			CP=V;
			if(CP[15]==ver && CP[14]==ssq && CP[10]==hor){
				swap(CP[11],CP[19]);//move 2
				L.push_back(CP);
			}else if ((CP[14]==ver|| CP[14]==hor) && CP[17]==ssq){
				swap(CP[17],CP[19]);//mov 21 //mov 79
				L.push_back(CP);
			}else if(CP[14]==ssq && CP[10]==ssq && CP[16]==hor){
				swap(CP[16],CP[18]);//mov 40
				swap(CP[17],CP[19]);
				L.push_back(CP);
			}
			break;
	}
	return L;
}

//check if we find the solution
bool isFinalGoal(vector<int> V)
{
	if (V[13]==lsq && V[14]==lsq && V[17]==lsq && V[18]==lsq){
		return true;
	}
	else{
		return false;
	}
}

//solution for problems - what i want to happen for given starting board
vector<vector<int>> list;
bool BFSsolution(vector<int> V)
{
	queue<vector<int>> Q;
	vector<int> source;//like a parent
	vector<vector<int>> newC; //new set of boards/candidate
	//vector<vector<int>> list; //list of boards
	Q.push(V);
	while(!Q.empty())
	{
		source = Q.front();
		Q.pop();
		list.push_back(source);//save vectors to list
		getEmptyIndex(source,EI);

		if (isFinalGoal(source)){
			printList(list);
			cout << "\nGot Solution!! \n\n";
			return true;
		}

		newC = myMove(EI,CM,source,listOfCan);//needs new list of boards
		for(vector<vector<int>>::iterator i = newC.begin(); i!= newC.end();i++){
			Q.push(*i);
		}
	}
	V = source;
	cout<< "\nNo solution!\n\n";
	return false; //no solution
}

//this will put my coordinates/index 0-19 of my board into vector
vector<int> myBoard(vector<int>& V){
	V.clear();
	for (int j = 0; j < 5; j++){
		int yy = bframe.y + j*(bframe.h/5) + ep;
		for (int i = 0; i< 4; i++){
			int xx = bframe.x + i*(bframe.w/4) + ep;
			if (findBlock(xx,yy)!=NULL)
			V.push_back(findBlock(xx,yy)->type);
			else V.push_back(0);
		}
	}
	return V;
}

//print board for debugging in case P
void printBoard(const vector<int> V){
	for (int i = 0; i < 4; i++){
		cout << V[i];
	}
	cout << '\n';
	for (int j = 4; j < 8; j++){
		cout << V[j];
	}
	cout << '\n';
	for (int k = 8; k < 12; k++){
		cout << V[k];
	}
	cout <<'\n';
	for (int l = 12; l < 16; l++){
		cout << V[l];
	}
	cout <<'\n';
	for (int m = 16; m < 20; m++){
		cout << V[m];
	}
	cout <<'\n';
}

void drawBlocks()
{
	/* rectangles */
	SDL_SetRenderDrawColor(gRenderer, 0x43, 0x4c, 0x5e, 0xff);
	for (size_t i = 0; i < 5; i++) {
		SDL_RenderFillRect(gRenderer,&B[i].R);
	}
	/* small squares */
	SDL_SetRenderDrawColor(gRenderer, 0x5e, 0x81, 0xac, 0xff);
	for (size_t i = 5; i < 9; i++) {
		SDL_RenderFillRect(gRenderer,&B[i].R);
	}
	/* large square */
	SDL_SetRenderDrawColor(gRenderer, 0xa3, 0xbe, 0x8c, 0xff);
	SDL_RenderFillRect(gRenderer,&B[9].R);
}

/* return a block containing (x,y), or NULL if none exists. */
block* findBlock(int x, int y)
{
	/* NOTE: we go backwards to be compatible with z-order */
	for (int i = NBLOCKS-1; i >= 0; i--) {
		if (B[i].R.x <= x && x <= B[i].R.x + B[i].R.w &&
				B[i].R.y <= y && y <= B[i].R.y + B[i].R.h)
			return (B+i);
	}
	return NULL;
}

void close()
{
	SDL_DestroyRenderer(gRenderer); gRenderer = NULL;
	SDL_DestroyWindow(gWindow); gWindow = NULL;
	SDL_Quit();
}

void render()
{
	/* draw entire screen to be black: */
	SDL_SetRenderDrawColor(gRenderer, 0x00, 0x00, 0x00, 0xff);
	SDL_RenderClear(gRenderer);

	/* first, draw the frame: */
	int& W = SCREEN_WIDTH;
	int& H = SCREEN_HEIGHT;
	int w = bframe.w;
	int h = bframe.h;
	SDL_SetRenderDrawColor(gRenderer, 0x39, 0x39, 0x39, 0xff);
	SDL_RenderDrawRect(gRenderer, &bframe);
	/* make a double frame */
	SDL_Rect rframe(bframe);
	int e = 3;
	rframe.x -= e;
	rframe.y -= e;
	rframe.w += 2*e;
	rframe.h += 2*e;
	SDL_RenderDrawRect(gRenderer, &rframe);

	/* draw some grid lines: */
	SDL_Point p1,p2;
	SDL_SetRenderDrawColor(gRenderer, 0x19, 0x19, 0x1a, 0xff);
	/* vertical */
	p1.x = (W-w)/2;
	p1.y = (H-h)/2;
	p2.x = p1.x;
	p2.y = p1.y + h;
	for (size_t i = 1; i < 4; i++) {
		p1.x += w/4;
		p2.x += w/4;
		SDL_RenderDrawLine(gRenderer,p1.x,p1.y,p2.x,p2.y);
	}
	/* horizontal */
	p1.x = (W-w)/2;
	p1.y = (H-h)/2;
	p2.x = p1.x + w;
	p2.y = p1.y;
	for (size_t i = 1; i < 5; i++) {
		p1.y += h/5;
		p2.y += h/5;
		SDL_RenderDrawLine(gRenderer,p1.x,p1.y,p2.x,p2.y);
	}
	SDL_SetRenderDrawColor(gRenderer, 0xd8, 0xde, 0xe9, 0x7f);
	SDL_Rect goal = {bframe.x + w/4 + ep, bframe.y + 3*h/5 + ep,
	                 w/2 - 2*ep, 2*h/5 - 2*ep};
	SDL_RenderDrawRect(gRenderer,&goal);

	/* now iterate through and draw the blocks */
	drawBlocks();
	/* finally render contents on screen, which should happen once every
	 * vsync for the display */
	SDL_RenderPresent(gRenderer);
}

void snap(block* b)
{
	/* TODO: once you have established a representation for configurations,
	 * you should update this function to make sure the configuration is
	 * updated when blocks are placed on the board, or taken off.  */
	assert(b != NULL);
	/* upper left of grid element (i,j) will be at
	 * bframe.{x,y} + (j*bframe.w/4,i*bframe.h/5) */
	/* translate the corner of the bounding box of the board to (0,0). */
	int x = b->R.x - bframe.x;
	int y = b->R.y - bframe.y;
	int uw = bframe.w/4;
	int uh = bframe.h/5;
	/* NOTE: in a perfect world, the above would be equal. */
	int i = (y+uh/2)/uh; /* row */
	int j = (x+uw/2)/uw; /* col */
	if (0 <= i && i < 5 && 0 <= j && j < 4) {
		b->R.x = bframe.x + j*uw + ep;
		b->R.y = bframe.y + i*uh + ep;
	}
}

int main(int argc, char *argv[])
{
	/* TODO: add option to specify starting state from cmd line? */
	/* start SDL; create window and such: */
	if(!init()) {
		printf( "Failed to initialize from main().\n" );
		return 1;
	}
	initConfig();
	atexit(close);
	bool quit = false; /* set this to exit main loop. */
	SDL_Event e;
	/* main loop: */
	while(!quit) {
		/* handle events */
		while(SDL_PollEvent(&e) != 0) {
			/* meta-q in i3, for example: */
			if(e.type == SDL_MOUSEMOTION) {
				if (mousestate == 1 && dragged) {
					int dx = e.button.x - lastm.x;
					int dy = e.button.y - lastm.y;
					lastm.x = e.button.x;
					lastm.y = e.button.y;
					dragged->R.x += dx;
					dragged->R.y += dy;
				}
			} else if (e.type == SDL_MOUSEBUTTONDOWN) {
				if (e.button.button == SDL_BUTTON_RIGHT) {
					block* b = findBlock(e.button.x,e.button.y);
					if (b) b->rotate();
				} else {
					mousestate = 1;
					lastm.x = e.button.x;
					lastm.y = e.button.y;
					dragged = findBlock(e.button.x,e.button.y);
				}
				/* XXX happens if during a drag, someone presses yet
				 * another mouse button??  Probably we should ignore it. */
			} else if (e.type == SDL_MOUSEBUTTONUP) {
				if (e.button.button == SDL_BUTTON_LEFT) {
					mousestate = 0;
					lastm.x = e.button.x;
					lastm.y = e.button.y;
					if (dragged) {
						/* snap to grid if nearest location is empty. */
						snap(dragged);
					}
					dragged = NULL;
				}
			} else if (e.type == SDL_QUIT) {
				quit = true;
			} else if (e.type == SDL_KEYDOWN) {
				switch (e.key.keysym.sym) {
					case SDLK_ESCAPE:
					case SDLK_q:
						quit = true;
						break;
					case SDLK_LEFT:
						/* TODO: show previous step of solution */
						break;
					case SDLK_RIGHT:
						/* TODO: show next step of solution */
						break;
					case SDLK_p:
						/* TODO: print the state to stdout
						 * (maybe for debugging purposes...) */
						myBoard(V);
						printBoard(V);
						getEmptyIndex(V,EI);
						cout << '\n';
						break;
					case SDLK_s:
						/* TODO: try to find a solution */
						myBoard(V);
						BFSsolution(V);
						break;
					default:
						break;
				}
			}
		}
		fcount++;
		render();
	}
	printf("total frames rendered: %i\n",fcount);
	return 0;
}

//NOTES:
/* First thing i did was to set the blocks in the middle
	through initial config. Then I decided to put the type
	of the blocks into a vector. I just followed the board
	through indices.

 * I knew from the beginning that BFS is the ideal way to use
	since we do not care about size of memory and we just want
	to get the solution as fast as possible. DFS would also work
	but would take longer steps. I created functions to find
	indices that are empty and are possible to move.

 * I had trouble in getting the list of possible candidates
	since i couldnt figure out a shorter way to move my data
	inside my vectors. I realize i spent so much time trying
	to find a way without writing all the possible conditions
	as you can see from the possible cases that i wrote. My BFS
	works when i tested with a small list but i also had an issue
	with the duplicates which made my BFS run infinitely. I have
	numerous attempts on the move function but decided to just do
	at least one  set working.

 * I think i made a mistake but putting them in just one vector
	instead of vector of vectors. I could not figure out how to
	draw them back to the board. Therefore, my solution just prints
	out in the standard output instead.

***************************************************************
 *  Thank you for everything professor! You made me appreciate
	computer science in great way! This class and assembly language
	are the only great online experience that i had this summer.
	Your lectures are great and the projects made me think and study! I
	know i wasn't able to completely finish this project but I'm glad
	that I did my best. I am thankful for being your student in both
	CSc 103 and Data structures! I wish you all the best!

*/